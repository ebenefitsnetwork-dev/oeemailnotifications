﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using log4net.Appender;
using log4net.Repository.Hierarchy;
using log4net;
using SpreadsheetLight;
using System.Text.RegularExpressions;

namespace emailnotificationforediorders
{
    class Program
    {
        public static log4net.ILog logger;
        public static DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0);//Unix Epoch on January 1st, 1970
        public static string logfilename;
        public static string logpath;
        #region Logging Functions
        static void LogString(string stringToLog)
        {
            logger.Info(stringToLog);
        }
        private static void InitializeLogger()
        {
            if (log4net.LogManager.GetCurrentLoggers().Length == 0)
            {
                RollingFileAppender fileLogRoot = new RollingFileAppender();
                log4net.GlobalContext.Properties["date"] = DateTime.Now;
                string path = AppDomain.CurrentDomain.BaseDirectory.ToString();
                string configFile = path + "App.config";
                log4net.Config.XmlConfigurator.Configure(new FileInfo(configFile));
            }
            logger = log4net.LogManager.GetLogger(typeof(Program));
            logger.Info("Begin processing.");
            var rootAppender = ((Hierarchy)LogManager.GetRepository())
                                         .Root.Appenders.OfType<FileAppender>()
                                         .FirstOrDefault();
            logpath = rootAppender != null ? rootAppender.File : string.Empty;
            logfilename = Path.GetFileName(logpath);
        }
        #endregion

        static void Main(string[] args)
        {

            DateTime start;
            TimeSpan duration = new TimeSpan(1, 12, 23, 62);

            InitializeLogger();
            start = DateTime.Now;
            logger.Info(DateTime.Now);

            Console.WriteLine("Application starts...");
            logger.Info("Application starts");


            // For EDI Orders 
            getEDIOrders();

            // OE Orders
            getOEOrders();


            getOrderCustomersAndCarriers();

            duration = DateTime.Now - start;
            string durationformat = duration.ToString();
            logger.Info("Duration: " + duration);
            logger.Info("Durationxxx: " + durationformat.Substring(0, durationformat.LastIndexOf(".")));
            logger.Info("Completed at: " + DateTime.Now);


        }
        // Getting EDI orders
        public static void getEDIOrders()
        {
            Console.WriteLine("kk");
            logger.Info("EDI");

            string path = Path.Combine(Path.GetDirectoryName(typeof(Program).Assembly.Location), "Content", "EmailNotificationsTemplateForEDIOrders.html");

            try
            {
                using (DataCustomerDataContext ddc = new DataCustomerDataContext())
                {
                    var cusList = (from C in ddc.Carriers
                                   join O in ddc.Orders on C.OrderID equals O.OrderID
                                   join CU in ddc.Customers on O.OrderID equals CU.OrderID
                                   join CO in ddc.Orders on O.OrderID equals CO.ParentOrderID
                                   into ChildO
                                   from COgrp in ChildO.DefaultIfEmpty()

                                   join CC in ddc.CustomerContacts on CU.CustomerID equals CC.CustomerID
                                   // join mc in ddc.o on CC.CustomerContactEmail equals U.Email 
                                   join MC in ddc.OrderMappingContacts on O.OrderID equals MC.OrderId
                                   into gCQ
                                   from CQ in gCQ.DefaultIfEmpty()

                                   join P in ddc.PartnerSettings on O.PartnerID equals P.PartnerID

                                   where (!O.PartnerID.Equals(150)) & C.StatusId.Equals(1) & CC.IsPartner.Equals(1)
                                   //& C.ServiceType.Equals(1)
                                   & O.OrderType.Equals(1) & O.StatusId.Equals(2)
                                   & (!CU.EnrollementMonth.Equals(null) || !CU.PlanYearStartDate.Equals(null))
                                   & (COgrp == null) & P.SettingsID.Equals(2)
                                   select new
                                   {
                                       EnrollementMonth = CU.EnrollementMonth,
                                       PlanYearStartDate = CU.PlanYearStartDate,
                                       CustomerName = CU.CustomerName,
                                       CustomerContactName = CC.CustomerContactName,
                                       CustomerContactEmail = CC.CustomerContactEmail,
                                       MappingContactName = CQ.ContactName,
                                       MappingContactEmail = CQ.ContactEmail,
                                       PartnerSettingID = P.SettingsID,
                                       NotificationDays = P.OEEmailNotificationDays
                                   }).ToList();

                    logger.Info("EDI Customers count: " + cusList.Distinct().Count());

                    foreach (var c in cusList.Distinct())
                    {
                        try
                        {
                            string to = c.CustomerContactEmail;
                            string from = ConfigurationManager.AppSettings["NotificationMail"];
                            string customerName = c.CustomerName;
                            string CustomerContactName = c.CustomerContactName;
                            string mailBody = "";
                            int NotificationDays = 0;
                            // Getting and assigning the PlanStart Date based on Enrollment Month
                            DateTime planStartDate = (!string.IsNullOrEmpty(c.EnrollementMonth) && c.EnrollementMonth.ToLower().Trim() != "null") ? new DateTime(DateTime.Now.Year, FindMonth(c.EnrollementMonth), 1) : Convert.ToDateTime(c.PlanYearStartDate);
                            // Getting Notification Days 
                            NotificationDays = (c.NotificationDays > 0) ? c.NotificationDays : (!string.IsNullOrEmpty(c.EnrollementMonth) && c.EnrollementMonth.ToLower().Trim() != "null") ? 30 : 90;

                            if (planStartDate.AddDays(-NotificationDays).ToString("dd/MM") == DateTime.Now.ToString("dd/MM"))
                            {
                                logger.Info("customerName, " + customerName + ", " + planStartDate);
                                mailBody = (System.IO.File.ReadAllText(path)).Replace("_PartnerContactFirstName", CustomerContactName).Replace("_X", Convert.ToString(NotificationDays)).Replace("_CustomerName", customerName);
                                mailSendFunctionlity(from, to, customerName + " Open Enrollment", mailBody, customerName);
                                if (!string.IsNullOrEmpty(c.MappingContactEmail))
                                {
                                    to = c.MappingContactEmail;
                                    mailBody = (System.IO.File.ReadAllText(path)).Replace("_PartnerContactFirstName", c.MappingContactName ?? "Customer").Replace("_X", Convert.ToString(NotificationDays)).Replace("_CustomerName", customerName);
                                    mailSendFunctionlity(from, to, customerName + " Open Enrollment", mailBody, customerName);
                                }
                            }


                            //if (!string.IsNullOrEmpty(c.EnrollementMonth) && c.EnrollementMonth.ToLower().Trim() != "null")
                            //{
                            //    logger.Info("customerName, " + customerName + ", " + c.EnrollementMonth);
                            //    DateTime enrollMonthStartDate = new DateTime(DateTime.Now.Year, FindMonth(c.EnrollementMonth), 1);
                            //    if (enrollMonthStartDate.AddDays(-30).ToString("dd/MM") == DateTime.Now.ToString("dd/MM"))
                            //    {
                            //        mailBody = (System.IO.File.ReadAllText(path)).Replace("_PartnerContactFirstName", CustomerContactName).Replace("_X", "30").Replace("_CustomerName", customerName);
                            //        mailSendFunctionlity(from, to, customerName + " Open Enrollment", mailBody, customerName);

                            //        if (!string.IsNullOrEmpty(c.MappingContactEmail))
                            //        {
                            //            to = c.MappingContactEmail;
                            //            mailBody = (System.IO.File.ReadAllText(path)).Replace("_PartnerContactFirstName", c.MappingContactName ?? "Customer").Replace("_X", "30").Replace("_CustomerName", customerName);
                            //            mailSendFunctionlity(from, to, customerName + " Open Enrollment", mailBody, customerName);

                            //        }
                            //    }
                            //}
                            //else
                            //{
                            //    DateTime planYearStart = Convert.ToDateTime(c.PlanYearStartDate);
                            //    if (planYearStart.AddDays(-90).ToString("dd/MM") == DateTime.Now.ToString("dd/MM"))
                            //    {
                            //        mailBody = (System.IO.File.ReadAllText(path)).Replace("_PartnerContactFirstName", CustomerContactName).Replace("_X", "90").Replace("_CustomerName", customerName);
                            //        mailSendFunctionlity(from, to, customerName + " Open Enrollment", mailBody, customerName);
                            //        if (!string.IsNullOrEmpty(c.MappingContactEmail))
                            //        {
                            //            to = c.MappingContactEmail;
                            //            mailBody = (System.IO.File.ReadAllText(path)).Replace("_PartnerContactFirstName", c.MappingContactName ?? "Customer").Replace("_X", "90").Replace("_CustomerName", customerName);
                            //            mailSendFunctionlity(from, to, customerName + " Open Enrollment", mailBody, customerName);
                            //        }
                            //    }
                            //}

                        }
                        catch (Exception ex)
                        {
                            logger.Error("Error obtainning EDI orders, Message: " + ex.Message.ToString() + " customerName:= " + c.CustomerName);
                            if (ex.InnerException != null) logger.Error("Error inner ex: " + ex.InnerException);

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("Error obtainning EDI orders, Message: " + ex.Message.ToString(), ex);
                if (ex.InnerException != null) logger.Error("Error inner ex: " + ex.InnerException);

            }
        }
        public static int FindMonth (string MonthName)
        {
            try
            {
                string[] allMonths = System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.MonthNames;               
                int month = Array.IndexOf(allMonths.Select(d => d.ToLower()).ToArray(), MonthName.ToLower());
                return month + 1;
            }
            catch(Exception ex)
            {
                return 0;
            }
        }
        // Getting the OE orders
        public static  void getOEOrders()
        {
            logger.Info("OE");
            string path = Path.Combine(Path.GetDirectoryName(typeof(Program).Assembly.Location), "Content", "EmailNotificationsTemplateForOEOrders.html");
            //DataTable oeTable = null;
            //SqlCommand sqlCmd;
            //SqlConnection sqlCnn;
            SqlDataAdapter adapter = new SqlDataAdapter();
            DataSet ds = new DataSet();
            StringBuilder sqlQuery = new StringBuilder();
            using (DataCustomerDataContext ddc = new DataCustomerDataContext())
            {
                var cusList = (from C in ddc.Carriers
                               join O in ddc.Orders on C.OrderID equals O.OrderID
                               join CU in ddc.Customers on O.ParentOrderID equals CU.OrderID
                               join CC in ddc.CustomerContacts on CU.CustomerID equals CC.CustomerID
                               //join U in ddc.UserProfiles on CC.CustomerContactEmail equals U.Email
                               join MC in ddc.OrderMappingContacts on O.ParentOrderID equals MC.OrderId
                               
                               into gCQ
                               from CQ in gCQ.DefaultIfEmpty()

                               where
                               (!O.PartnerID.Equals(150))
                               & ((C.StatusId.Equals(1) | C.ServiceType.Equals(2)) & CC.IsPartner.Equals(1)
                               &( C.ServiceType.Equals(2) || C.ServiceType.Equals(3) )
                               & O.OrderType.Equals(2) & O.StatusId.Equals(2) )
                               // & (!CU.EnrollementMonth.Equals(null) || !CU.PlanYearStartDate.Equals(null))
                                

                               select new
                               {
                               OEStartDate = C.OEStartDate,
                               CustomerName = CU.CustomerName,
                               CustomerContactName = CC.CustomerContactName,
                               CustomerContactEmail = CC.CustomerContactEmail,
                               MappingContactName = CQ.ContactName,
                               MappingContactEmail = CQ.ContactEmail
                               }).ToList();

                foreach (var c in cusList.Distinct())
                {
                    string to = c.CustomerContactEmail;
                    string from = ConfigurationManager.AppSettings["NotificationMail"];
                    string customerName = c.CustomerName;
                    string CustomerContactName = c.CustomerContactName;
                    string mailBody = "";
                    if (c.OEStartDate != null)
                    {
                        if (c.OEStartDate.Value.AddDays(-45).ToString("dd/MM") == DateTime.Now.ToString("dd/MM"))
                        {
                             mailBody = (System.IO.File.ReadAllText(path)).Replace("_PartnerContactFirstName", CustomerContactName).Replace("_X", "45").Replace("_CustomerName", customerName);
                            mailSendFunctionlity(from, to, customerName +" Open Enrollment", mailBody, customerName);

                            if (!string.IsNullOrEmpty(c.MappingContactEmail))
                            {
                                to = c.MappingContactEmail;
                                mailBody = (System.IO.File.ReadAllText(path)).Replace("_PartnerContactFirstName", c.MappingContactName?? "Customer").Replace("_X", "45").Replace("_CustomerName", customerName);
                                mailSendFunctionlity(from, to, customerName + " Open Enrollment", mailBody, customerName);

                            }

                        }
                    }
                }
            }

            #region commented
            //sqlQuery.Append("SELECT DISTINCT O.ORDERID, C.OEStartDate, CC.ISPARTNER, CU.CUSTOMERID, c.CarrierID, c.statusid,");
            //sqlQuery.Append("CC.CustomerContactEmail As  EmailID, CC.CustomerID, CU.CustomerName, CC.CustomerContactName,");
            //sqlQuery.Append("DATEDIFF(day, FORMAT(GETDATE(), 'dd/MMM') , FORMAT(C.OEStartDate, 'dd/MMM')) As difdate  ");
            //sqlQuery.Append("FROM [dbo].[Carrier] C INNER JOIN [dbo].[Order] O ");
            //sqlQuery.Append("ON C.ORDERID = O.ORDERID INNER JOIN [dbo].[Customer] CU  ON CU.ORDERID = O.ORDERID ");
            //sqlQuery.Append("INNER JOIN [dbo].[CustomerContact] CC ON CC.CUSTOMERID = CU.CUSTOMERID ");
            //sqlQuery.Append("INNER JOIN [dbo].[UserProfile] U ON U.Email = CC.CustomerContactEmail ");
            //sqlQuery.Append("WHERE C.OEStartDate Is Not Null AND DATEDIFF(day, FORMAT(GETDATE(), 'dd/MMM') , FORMAT(C.OEStartDate, 'dd/MMM')) = 45  ");
            //sqlQuery.Append("AND O.OrderType=2  and c.StatusId not in (3,4) and o.StatusId = 2 and cc.IsPartner = 1 ");

            //string connStr = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
            //sqlCnn = new SqlConnection(connStr);
            //try
            //{
            //    sqlCnn.Open();
            //    sqlCmd = new SqlCommand(sqlQuery.ToString(), sqlCnn);
            //    adapter.SelectCommand = sqlCmd;
            //    adapter.Fill(ds);
            //    sqlCnn.Close();
            //    sqlCnn.Dispose();
            //    sqlCmd.Dispose();
            //    adapter.Dispose();
            //    return ds.Tables[0];
            //}
            //catch (Exception ex)
            //{
            //    logger.Error("Error obtainning OE orders, Message: " + ex.Message.ToString(), ex);
            //    if (ex.InnerException != null) logger.Error("Error inner ex: " + ex.InnerException);

            //    return oeTable;
            //}
            #endregion
        }
 
        // OE order mail send functionality
        static void sendMail(DataTable dt)
        {
            string path = Path.Combine(Path.GetDirectoryName(typeof(Program).Assembly.Location), "Content", "EmailNotificationsTemplateForOEOrders.html");

            // log4net for Error logs
            string to = " ";
            string customerName = "";
            try
            {
                if (dt != null)
                {
                    logger.Info("OE Customers count: " + dt.Rows.Count);


                    string from = "notifications@ebenefitsnetwork.com";
                    foreach (DataRow row in dt.Rows)
                    {
                        if (row["EmailID"] != DBNull.Value)
                        {
                            to = Convert.ToString(row["EmailID"]);
                            customerName = Convert.ToString(row["CustomerName"]);
                            string mailBody = (System.IO.File.ReadAllText(path)).Replace("_PartnerContactFirstName", Convert.ToString(row["CustomerContactName"])).Replace("_X", Convert.ToString(row["difdate"])).Replace("_CustomerName", Convert.ToString(row["CustomerName"]));
                            mailSendFunctionlity(from, to, "Email Notification for OE Orders", mailBody, customerName);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("Error Message: " + ex.Message.ToString(), ex);
            }
        }
      
        // Mail sending functionaity for both
        static void mailSendFunctionlity(string from, string to, string subject, string body, string customerName, string AttachedFile = null)
        {          
            // Smtp settings
            SmtpClient client = new SmtpClient();
            // string to = Convert.ToString(row["EmailID"]);
            // to = "Karguvelrajan.P@gmail.com";
            MailMessage message = new MailMessage(from, to);
            message.Subject = subject;
            message.Body = body;

            if (AttachedFile != null)
            {
                message.Attachments.Add(new Attachment(AttachedFile));
            }

            message.IsBodyHtml = true;
            try
            {
                client.Send(message);
                message.Dispose();
                client.Dispose();
                logger.Info("Success: Customer Name : " + customerName + ", EmailID : " + to );

            }
            catch (Exception ex)
            {
                message.Dispose();
                client.Dispose();
                logger.Error("Error: Customer Name : " + customerName + ", EmailID : " + to + " Error Message: " + ex.Message.ToString(), ex);
            }
        }

        public static void getOrderCustomersAndCarriers()
        {
            logger.Info("getOrderCustomersAndCarriers");
            if (DateTime.Now.Day == 15)
            {
                logger.Info("today is 15 of the month");

                using (DataCustomerDataContext dc = new DataCustomerDataContext())
                {
                    var PartnerList = dc.GetPartnerNameBasedOnSettings(1).ToList();

                    string from = ConfigurationManager.AppSettings["NotificationMail"];

                    foreach (var p in PartnerList)
                    {
                        var CustomerList = dc.GetCustomerAndCarriersName(p.PartnerId).ToList();

                        string path = "", mailbody = "", attached = "";
                        List<string> ReciepientMails = QBase.getReciepientMails(p.PartnerName.ToLower());


                        string[] to;

                        if (CustomerList.Count() != 0)
                        {
                            SLDocument sl = new SLDocument();

                            sl.SetCellValue(1, 1, "Customer Name");
                            sl.SetCellValue(1, 2, "Carrier Name");
                            sl.SetCellValue(1, 3, "Plan year start date");

                            for (int index = 0; index < CustomerList.Count(); index++)
                            {
                                sl.SetCellValue(index + 2, 1, CustomerList[index].CustomerName);
                                sl.SetCellValue(index + 2, 2, CustomerList[index].CarrierName);
                                sl.SetCellValue(index + 2, 3, CustomerList[index].PlanYearStartDate.Value.ToShortDateString());
                            }

                            if (!Directory.Exists("OEBulkExcel"))
                                Directory.CreateDirectory("OEBulkExcel");

                            sl.SaveAs(@"OEBulkExcel\CustomersAndCarriers_" + p.PartnerName.Trim() +"_" + DateTime.Now.ToShortDateString().Replace("/", "") + ".xlsx");

                            path = Path.Combine(Path.GetDirectoryName(typeof(Program).Assembly.Location), "Content", "OEProjectstemplate.html");
                            mailbody = (System.IO.File.ReadAllText(path.Replace("//", "/")));
                            attached = @"OEBulkExcel\CustomersAndCarriers_" + p.PartnerName.Trim() + "_" + DateTime.Now.ToShortDateString().Replace("/", "") + ".xlsx";
                        }
                        else
                        {
                            path = Path.Combine(Path.GetDirectoryName(typeof(Program).Assembly.Location), "Content", "NoOEProjectstemplate.html");
                            mailbody = (System.IO.File.ReadAllText(path));
                            attached = null;
                        }

                        if (ReciepientMails.Count() != 0)
                        {
                            foreach (string mail in ReciepientMails)
                            {
                                to = mail.Trim().Split(';');
                                foreach (string ReciepientMail in to)
                                {
                                    string ReciepientMailtrimmed = ReciepientMail.Trim().TrimStart('\r', '\n').TrimEnd('\r', '\n');

                                    try
                                    {
                                        mailSendFunctionlity(from, ReciepientMailtrimmed, "Automatic OE Orders Report - " + p.PartnerName.Trim(), mailbody, p.PartnerName.Trim(), attached);

                                        logger.Info("success: send Bulkemail to" + ReciepientMailtrimmed);
                                    }
                                    catch (Exception ex)
                                    {
                                        logger.Error("Failed: send Bulkemail to" + ReciepientMailtrimmed);
                                        logger.Error("Failed: exception" + ex.Message);
                                        if (ex.InnerException != null) logger.Error("Failed: inner exception" + ex.InnerException.ToString());

                                    }
                                }
                            }
                        }

                        else
                            logger.Info("no reciepients were found");
                    }
                }
            }
        }


    }
}
