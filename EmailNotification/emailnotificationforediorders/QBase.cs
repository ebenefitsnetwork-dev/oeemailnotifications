﻿using eBNQuickBaseLibrary;
using log4net.Repository.Hierarchy;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace emailnotificationforediorders
{
    public static class QBase
    {

        public  static List<string> getReciepientMails(string ParnterName)
        {
            List<string> ReciepientMails = new List<string>();

            string username = ConfigurationManager.AppSettings["QBUserName"];
            string Password = ConfigurationManager.AppSettings["QBPassword"];
            string SDomain = ConfigurationManager.AppSettings["QBDomain"];
            string TicketsTable = ConfigurationManager.AppSettings["PortalReports"];

            try
            {
                using (var connection = new QuickBaseConnection(SDomain, username, Password))
                {
                    var ticketQuery = FormTicketQuery("Automatic OE Orders Report", ParnterName); 
                    var datacount = connection.DoQueryCount(TicketsTable, ticketQuery);
                    if (datacount > 0)
                    {
                        DataTable dataTable = connection.DoQueryAndFillTable(TicketsTable, ticketQuery, new int[] { 7 }.ToList(), null);
                        foreach (DataRow row in dataTable.Rows)
                        {
                            ReciepientMails.Add(row.ItemArray[0].ToString());
                        }
                    }
                   
                }
            }

            catch (Exception ex)
            {

            }

            return ReciepientMails;
        }

        public static QuickBaseMultipleQuery FormTicketQuery(string ReportName, string ParnterName)
        {
            var ticketQuery = new QuickBaseMultipleQuery { AndOrOperator = QuickBaseAndOrOperator.AND };
            ticketQuery.Queries.Add(new QuickBaseSingleQuery() { FieldId = 6, ComparisonOperator = QuickBaseComparisonOperator.CT, Value = ReportName });
        
            //testing
           // ticketQuery.Queries.Add(new QuickBaseSingleQuery() { FieldId = 8, ComparisonOperator = QuickBaseComparisonOperator.EX, Value = ParnterName });


            //Production 
            ticketQuery.Queries.Add(new QuickBaseSingleQuery() { FieldId = 9, ComparisonOperator = QuickBaseComparisonOperator.EX, Value = ParnterName });

            return ticketQuery;
        }
    }
}
